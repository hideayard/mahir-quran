
function ajaxLogout() {
    var $ = jQuery;
    $.ajax({
    type: "POST",
    url: "logout.php",
    success: function(response) {
        try{
            rv = JSON.parse(response);
            if(isEmpty(rv) || rv.status==false)
            {
              myApp.alert(
                  'error!',
                  'Error NO DATA, '+rv.messages
                  );
                console.log("NO DATA : ", response);
            }
            else
            {
              if(rv.status==true)
              {
                myApp.alert(
                  'Success!',
                  'Success Logout!'
                  );
                console.log("SUCCESS : ", rv);
                deleteAllCookies();
                setTimeout(function(){ window.location="index.php"; }, 1000);
              }

            }
      }   
      catch (e) {
        myApp.alert(
                  'error!',
                  'Error Input Data, '+e
                  );
        console.log("ERROR : ", e);
      }

    }
    });
    return false;
    }