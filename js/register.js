

function ajaxRegister(theForm) {
        var $ = jQuery;
        $('#loader').fadeIn();
        var formData = $(theForm).serialize(),
        note = $('#Note');
        $.ajax({
        type: "POST",
        url: "actionregister.php",
        data: formData,
        success: function(response) {
    
            try{
                rv = JSON.parse(response);
                if(isEmpty(rv) || rv.status==false)
                {
                  myApp.alert(
                      'error!',
                      rv.messages,
                      );
                    console.log("Error : ", response);
                }
                else
                {
                  if(rv.status==true)
                  {
                    myApp.alert(
                      'Success!',
                      'Success Register!',
                      );
                    console.log("SUCCESS : ", rv);
                    document.cookie = "token="+rv.info;
                    setTimeout(function(){ window.location="index.php"; }, 1000);
                  }
    
                }
          }   
          catch (e) {
            
            myApp.alert(
                      'error!',
                      'Error Data, '+e,
                      );
          
        console.log("ERROR : ", e);
    
          }
    
        if ( note.height() ) {			
        note.fadeIn('fast', function() { $(this).hide(); });
        } else {
        note.hide();
        }
        $('#LoadingGraphic').fadeOut('fast', function() {
            if(rv.status==true) {
            
            $('.page_subtitle').hide();
            
        }

        note.removeClass('success').removeClass('error').text('');
     
        }); // end loading image fadeOut
        }

        });//end ajax
    return false;
}