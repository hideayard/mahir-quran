

function ajaxProfile(theForm) {
  console.log("ajaxProfile");
    var $ = jQuery;
    $('#loader').fadeIn();
    note = $('#Note');

    var form = $(theForm)[0];
    // Create an FormData object
    var data = new FormData(form);
    var files = $('#user_foto')[0].files;
    // console.log("files = ",files);
    if(files.length > 0)
    {
      console.log("ada files = ",files[0]);
      data.append("user_foto", files[0] );
    }
    
    $.ajax({
      type: "POST",
      enctype: 'multipart/form-data',
      url: "actionprofile.php",
      data: data,
      processData: false,
      contentType: false,
      cache: false,
      timeout: 600000,
    success: function(response) {

        try{
            let rv = JSON.parse(response);
            if(isEmpty(rv) || rv.status==false)
            {
              myApp.alert(
                  'error!',
                  rv.messages,
                  );
                console.log("Error : ", response);
            }
            else
            {
              if(rv.status==true)
              {
                myApp.alert(
                  'Success!',
                  'Success Update Profile!',
                  );
                console.log("SUCCESS : ", rv);
                // setTimeout(function(){ window.location.reload(); }, 1000);
              }

            }
      }   
      catch (e) {
        
        myApp.alert(
                  'error!',
                  'Error Data, '+e,
                  );
      
    console.log("ERROR : ", e);

      }

    if ( note.height() ) {			
    note.fadeIn('fast', function() { $(this).hide(); });
    } else {
    note.hide();
    }
    $('#LoadingGraphic').fadeOut('fast', function() {
        if(rv.status==true) {
        
        $('.page_subtitle').hide();
        
    }

    note.removeClass('success').removeClass('error').text('');
 
    }); // end loading image fadeOut
    }

    });//end ajax
return false;
}