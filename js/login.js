function isEmpty(obj) {
    for(var prop in obj) {
        if(obj.hasOwnProperty(prop))
            return false;
    }
      return true;
  }
  
  function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+ d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
  }

  function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
      var c = ca[i];
      while (c.charAt(0) == ' ') {
        c = c.substring(1);
      }
      if (c.indexOf(name) == 0) {
        return c.substring(name.length, c.length);
      }
    }
    return "";
  }

  function deleteAllCookies() {
    var cookies = document.cookie.split(";");

    for (var i = 0; i < cookies.length; i++) {
        var cookie = cookies[i];
        var eqPos = cookie.indexOf("=");
        var name = eqPos > -1 ? cookie.substr(0, eqPos) : cookie;
        document.cookie = name + "=;expires=Thu, 01 Jan 1970 00:00:00 GMT";
    }
}

function parseJwt (token) {
  var base64Url = token.split('.')[1];
  var base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
  var jsonPayload = decodeURIComponent(atob(base64).split('').map(function(c) {
      return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
  }).join(''));

  return JSON.parse(jsonPayload);
};

function ajaxLogin(theForm) {
    var $ = jQuery;
    $('#loader').fadeIn();
    var formData = $(theForm).serialize(),
    note = $('#Note');
    $.ajax({
    type: "POST",
    url: "actionlogin.php",
    data: formData,
    success: function(response) {

        try{
            rv = JSON.parse(response);
            if(isEmpty(rv) || rv.status==false)
            {
              myApp.alert(
                  'error!',
                  'Error NO DATA, '+rv.messages,
                  'error'
                  );
                console.log("NO DATA : ", response);
                // $("#error").fadeIn(500, function(){                        
                // $("#error").html('<div class="alert alert-danger"> <span class="glyphicon glyphicon-info-sign"></span> &nbsp; Username Or Password is Wrong !</div>');});
                $("#btn-login").html('<span class="glyphicon glyphicon-log-in"></span> &nbsp; Sign In');
                        
            }
            else
            {
              if(rv.status==true)
              {
                myApp.alert(
                  'Success!',
                  'Success Login!',
                  'success'
                  );
                console.log("SUCCESS : ", rv);
                document.cookie = "token="+rv.info;
                //set cookie
                setCookie('token',rv.info,1);
                // setTimeout(function(){ window.location="index.php?token="+rv.info; }, 1000);
                setTimeout(function(){ window.location="index.php"; }, 1000);
                // window.location="index.php";
              }

            }
      }   
      catch (e) {
        
        myApp.alert(
                  'error!',
                  'Error Input Data, '+e,
                  'error'
                  );
                //   $("#btn-login").html('<i class="fa fa-sync fa-spin"></i> &nbsp; Sign In');
      
    console.log("ERROR : ", e);

      }

    if ( note.height() ) {			
    note.fadeIn('fast', function() { $(this).hide(); });
    } else {
    note.hide();
    }
    $('#LoadingGraphic').fadeOut('fast', function() {
        if(rv.status==true) {
        
        $('.page_subtitle').hide();
        
    }
    // Message Sent? Show the 'Thank You' message and hide the form
    // var result = '';
    // var c = '';
    // if(rv.status==true) { 
    //     myApp.alert('Thank you for getting in touch.', 'Message sent!');
    //     c = 'success';
    // } else {
    //     result = response;
    //     c = 'error';
    // }
    note.removeClass('success').removeClass('error').text('');
    // var i = setInterval(function() {
    //     if ( !note.is(':visible') ) {
    //         note.html(result).addClass(c).slideDown('fast');
    //         clearInterval(i);
    //     }
    // }, 40);    
    }); // end loading image fadeOut
    }
    });
    return false;
    }


    function ajaxGetUser(token) {
      var $ = jQuery;
      $('#loader').fadeIn();
      note = $('#Note');
      $.ajax({
      type: "POST",
      url: "actiongetuser.php",
      data: token,
      success: function(response) {
  
          try{
              rv = JSON.parse(response);
              if(isEmpty(rv) || rv.status==false)
              {
                myApp.alert(
                    'error!',
                    'Error NO DATA, '+rv.messages,
                    'error'
                    );
                  console.log("NO DATA : ", response);
                  // $("#error").fadeIn(500, function(){                        
                  // $("#error").html('<div class="alert alert-danger"> <span class="glyphicon glyphicon-info-sign"></span> &nbsp; Username Or Password is Wrong !</div>');});
                  $("#btn-login").html('<span class="glyphicon glyphicon-log-in"></span> &nbsp; Sign In');
                          
              }
              else
              {
                if(rv.status==true)
                {
                  myApp.alert(
                    'Success!',
                    'Success Login!',
                    'success'
                    );
                  console.log("SUCCESS : ", rv);
                  document.cookie = "token="+rv.info;
                  //set cookie
                  setCookie('token',rv.info,1);
                  // setTimeout(function(){ window.location="index.php?token="+rv.info; }, 1000);
                  setTimeout(function(){ window.location="index.php"; }, 1000);
                  // window.location="index.php";
                }
  
              }
        }   
        catch (e) {
          
          myApp.alert(
                    'error!',
                    'Error Input Data, '+e,
                    'error'
                    );
                  //   $("#btn-login").html('<i class="fa fa-sync fa-spin"></i> &nbsp; Sign In');
        
      console.log("ERROR : ", e);
  
        }
  
      if ( note.height() ) {			
      note.fadeIn('fast', function() { $(this).hide(); });
      } else {
      note.hide();
      }
      $('#LoadingGraphic').fadeOut('fast', function() {
          if(rv.status==true) {
          
          $('.page_subtitle').hide();
          
      }
      // Message Sent? Show the 'Thank You' message and hide the form
      // var result = '';
      // var c = '';
      // if(rv.status==true) { 
      //     myApp.alert('Thank you for getting in touch.', 'Message sent!');
      //     c = 'success';
      // } else {
      //     result = response;
      //     c = 'error';
      // }
      note.removeClass('success').removeClass('error').text('');
      // var i = setInterval(function() {
      //     if ( !note.is(':visible') ) {
      //         note.html(result).addClass(c).slideDown('fast');
      //         clearInterval(i);
      //     }
      // }, 40);    
      }); // end loading image fadeOut
      }
      });
      return false;
      }