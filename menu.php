<?php
if (session_status() === PHP_SESSION_NONE) {
    session_start();
}
?>
<div class="pages">
  <div data-page="projects" class="page no-toolbar no-navbar">
    <div class="page-content">
    
      <div class="page_content_menu">
       <nav class="main-nav">
        <ul>
        <li><a href="index.php"><img src="images/icons/white/home.png" alt="" title="" /><span>Home</span></a></li>
        <?php
          if(isset($_SESSION['u']))
          {
            echo '<li><a href="profile.php"><img src="images/icons/white/user.png" alt="" title="" /><span>Profile</span></a></li>';
          }
          else
          {
            echo '<li><a href="#" data-popup=".popup-login" class="open-popup"><img src="images/icons/white/user.png" alt="" title="" /><span>Profile</span></a></li>';
          }
        ?>
        <li><a href="booking.php"><img src="images/icons/white/blog.png" alt="" title="" /><span>Booking</span></a></li>
        <li><a href="#"><img src="images/icons/white/history.png" alt="" title="" /><span>History</span></a></li>
        <li><a href="#"><img src="images/icons/white/settings.png" alt="" title="" /><span>Pengaturan</span></a></li>
        <li><a href="#"><img src="images/icons/white/payment.png" alt="" title="" /><span>Payment</span></a></li>
         
          <li><a href="contact.php"><img src="images/icons/white/phone.png" alt="" title="" /><span>Hubungi Kami</span></a></li>

          <?php
          if(isset($_SESSION['u']))
          {
            echo '<li><a  onclick="ajaxLogout();" href="#" class="close-panel btnLogout"><img src="images/icons/white/menu_close.png" alt="" title="" /><span>Logout</span></a></li>';
          }
          else
          {
            echo '<li><a href="#" data-popup=".popup-login" class="open-popup"><img src="images/icons/white/lock.png" alt="" title="" /><span>Login</span></a></li>';
          }
          ?>
        </ul>
      </nav>  
      <div class="close_popup_button"><a href="#" class="backbutton"><img src="images/icons/white/menu_close.png" alt="" title="" /></a></div>
     </div> 
      
    </div>
  </div>
</div>