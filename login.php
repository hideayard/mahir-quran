    <!-- Login Popup -->
<div class="popup popup-login">
  <div class="content-block-login">
    <h4>LOGIN</h4>
    
    <div class="form_logo"><img src="images/logo.png" alt="" title="" /></div>
    <h2 id="Note"></h2>
    <div class="loginform">

      <form id="LoginForm" method="post">
        <input type="text" name="username" value="" class="form_input required" placeholder="username" />
        <input type="password" name="password" value="" class="form_input required" placeholder="password" />
        <div class="forgot_pass"><a href="#" data-popup=".popup-forgot" class="open-popup">Forgot Password?</a></div>
        <label id="loader" style="display:none;"><img src="images/loader.gif" alt="Loading..." id="LoadingGraphic" /></label>
        <input type="submit" name="submit" class="form_submit" id="submitLogin" value="SIGN IN" />
          
      </form>
      <div class="signup_bottom">
        <p>Don't have an account?</p>
        <a href="#" data-popup=".popup-signup" class="open-popup">SIGN UP</a>            
      </div>

    </div>

    <div class="close_loginpopup_button"><a href="#" class="close-popup"><img src="images/icons/white/menu_close.png" alt="" title="" /></a></div>
  </div>
</div>

   