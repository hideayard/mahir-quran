<?php
if(!isset($_SESSION)) 
{ 
	if (session_status() === PHP_SESSION_NONE) {
    session_start();
} 
} 
	unset($_SESSION['i']);
	unset($_SESSION['u']);
	unset($_SESSION['e']);
	unset($_SESSION['t']);
	// unset cookies
	if (isset($_SERVER['HTTP_COOKIE'])) {
		$cookies = explode(';', $_SERVER['HTTP_COOKIE']);
		foreach($cookies as $cookie) {
			$parts = explode('=', $cookie);
			$name = trim($parts[0]);
			setcookie($name, '', time()-1000);
			setcookie($name, '', time()-1000, '/');
		}
	}
	
	if(session_destroy())
	{
		// header("Location: login.php");
		// exit(); //hentikan eksekusi kode di login_proses.php
		echo json_encode( array("status" => true,"info" => "Logout Sukses","messages" => "Logout Sukses" ) ); 
	}
	else
	{
		echo json_encode( array("status" => false,"info" => "Logout Gagal","messages" => "Logout Gagal" ) ); 
	}
?>

