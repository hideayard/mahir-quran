<?php

if( $uid != 0 )
{
    if($resultUser[0]['user_foto'] != "" && $resultUser[0]['user_foto'] != null)
    {
        $foto = 'uploads/user/'.$resultUser[0]['user_foto'];
    }
    else
    {
        $foto = 'images/avatar5.png';
    }
    

?>
<div class="panel panel-left panel-cover">
          <div class="user_login_info">
            <div class="user_thumb">
                <img src="<?=$foto?>" alt="profile"/>
                <div class="user_details">
                <p>Hi, <span><?=$resultUser[0]['user_nama']?></span></p>
                </div>  
                <div class="user_social">
                <!-- <a href="#" data-popup=".popup-social" class="open-popup"><img src="images/icons/white/twitter.png" alt="" title="" /></a>               -->
                </div>       
            </div>

                  <nav class="user-nav">
                    <ul>
                      <li><a href="#" class="close-panel"><img src="images/icons/white/settings.png" alt="" title="" /><span>Settings</span></a></li>
                      <li><a href="profile.php" class="close-panel"><img src="images/icons/white/briefcase.png" alt="" title="" /><span>Account</span></a></li>
                      <li><a href="#" class="close-panel"><img src="images/icons/white/message.png" alt="" title="" /><span>Messages</span><strong class="green">12</strong></a></li>
                      <li><a href="#" class="close-panel"><img src="images/icons/white/download.png" alt="" title="" /><span>Order</span><strong class="blue">5</strong></a></li>
                      <li><a  onclick="ajaxLogout();" href="#" class="close-panel btnLogout"><img src="images/icons/white/lock.png" alt="" title="" /><span>Logout</span></a></li>
                      <li><a onclick="refreshPage();" href="#" class="close-panel"><img src="images/icons/white/refresh.png" alt="" title="" /><span>Refresh</span></a></li>
                    </ul>
                  </nav>
          </div>
    </div>



<?php
}
else
{
?>
<div class="panel panel-left panel-cover">
    <div class="user_login_info">
        <nav class="user-nav">
            <ul>
            <li class="menuicon"><a href="menu.php"><img src="images/icons/white/menu.png" alt="" title="" /><span>All menu</span></a></li>
            <li><a onclick="refreshPage();" href="#" class="close-panel"><img src="images/icons/white/refresh.png" alt="" title="" /><span>Refresh</span></a></li>
            </ul>
        </nav>
    </div>
</div>
<?php        
}
?>

<script>
function refreshPage()
{
    localStorage.clear();
    window.location.reload();
}
</script>