<?php
if (session_status() === PHP_SESSION_NONE) {
    session_start();
}

 $sql = "SELECT * FROM metode WHERE status = 1 "; 
 $resultMetode = $db->rawQuery($sql);//@mysql_query($sql);
 $i=1;
?>
   <!-- Register Popup -->
    <div class="popup popup-signup">
        <div class="content-block-login">
          <h4>REGISTER FORM</h4>
          <div class="form_logo"><img src="images/logo.png" alt="" title="" /></div>
            <div class="loginform">
              <form id="RegisterForm" method="post">
    
                <label>Daftar Sebagai:</label>
                <div class="selector_overlay">
                  <select id="registerAs" name="user_tipe" class="cs-select cs-skin-overlay selectoptions registerAs custom-dropdown">
                    <!-- <option value="" disabled="disabled" selected>Daftar sebagai:</option> -->
                    <option value="SANTRI"><strong>Santri</strong></option><option value="USTADZ">Ustadz</option>
                  </select>
                </div>
                <br>
                
                <label>Username:</label>
                <input type="text" name="user_name" value="" class="form_input required" placeholder="username"/>
    
                <label>Password:</label>
                <input type="password" name="user_pass" value="" class="form_input required" placeholder="password"/>
    
                <label>Nama Lengkap:</label>
                <input type="text" name="user_nama" value="" class="form_input required" placeholder="nama lengkap"/>
    
                <label>Email:</label>
                <input type="text" name="user_email" value="" class="form_input required"placeholder="email" />
    
                <label>No HP:</label>
                <input type="text" name="user_hp" value="" class="form_input required" placeholder="no hp"/>

                <label>Jenis Kelamin:</label>
                <div class="selector_overlay">
                  <select name="user_kelamin" class="cs-select cs-skin-overlay selectoptions">
                    <option value="" disabled="disabled" selected>Jenis Kelamin:</option>
                    <option>LAKI-LAKI</option><option>PEREMPUAN</option>
                  </select>
                </div>	
                <br>
                <div id="divkemampuan" style="display:none;">
                    <label>Kemampuan:</label>
                    <?php
                            foreach($resultMetode as $key => $value)
                            {
                              if($key == 0)
                              {
                                $checked = 'checked="checked"';
                              }
                              else{
                                $checked = "";
                              }
                              

                            echo ' <label class="label-checkbox item-content">
                            <input type="checkbox" name="user_kemampuan'.($key+1).'" value="'.$value['nama'].'" '.$checked.'>
                            <div class="item-media"> <i class="icon icon-form-checkbox"></i> </div>
                            <div class="item-inner"> <div class="item-title">'.$value['label'].'</div> </div>
                            </label>';
                            }
                    ?>
                   
                </div>
                <br>
                <label id="loader" style="display:none;"><img src="images/loader.gif" alt="Loading..." id="LoadingGraphic" /></label>

                <input type="submit" name="submit" class="form_submit" id="submitRegister" value="Daftar" /><hr>
                <!-- <div class="table_section"><a href="#" class="col button button-large button-fill button-raised color-green">Update</a></div> -->
                <a onclick="toHome();" href="#" class="col button button-large button-fill button-raised color-green">Cancel</a>
                <!-- <a  onclick="toHome();" href="#" class="close-panel btnLogout"><img src="images/icons/white/lock.png" alt="" title="" /><span>Logout</span></a> -->
                <!-- <input type="submit" name="cancel" class="button " id="cancel" value="Cancel" /> -->
              </form>
            </div>
               
          <div class="close_loginpopup_button"><a href="#" class="close-popup"><img src="images/icons/white/menu_close.png" alt="" title="" /></a></div>
        </div>
    </div>
    <script>
   function toHome()
	{
		console.log("toHome");
    window.location="index.php";
	}
   </script>