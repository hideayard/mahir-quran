
<?php
// if (session_status() === PHP_SESSION_NONE) {
//     session_start();
// }

// require_once ('config/MysqliDb.php');
// include_once ("config/db.php");
// $db = new MysqliDb ('localhost', $dbuser, $dbpass, $dbname);
// include("config/functions.php");
// require_once ("jwt_token.php");
// require_once ("customhelper.php");
// var_dump($_COOKIE['token']);die;

// $vtoken = json_decode( verify_token($_COOKIE['token']) );
// var_dump($vtoken);
// if($vtoken->data->uid)
// {
//    $uid = $vtoken->data->uid;
// }
// else
// {
//    $uid = 0;
// }

//  $sql = "SELECT * FROM users WHERE user_id = '". $uid ."'"; 
//  $result = $db->rawQuery($sql);//@mysql_query($sql);
//  $i=1;

// $sql = "SELECT * FROM users WHERE user_id = '".$_SESSION['i']."' "; 
// $resultUserUser = $db->rawQuery($sql);//@mysql_query($sql);

?>
   <!-- Register Popup -->
    <div class="popup popup-profile">
        <div class="page">
          <h4>Profile User</h4>

          <div class="buttons-row">
               <a href="#tinfo" class="tab-link active button">User Info</a>
               <a href="#tsert" class="tab-link button">Skill</a>
               <a href="#tpayment" class="tab-link button">Payment</a>
         </div>  


          <div class="tabs-animated-wrap">
                    <div class="tabs">
                          <div id="tinfo" class="tab active">
                              
                              <div class="loginform">
                              <form id="ProfileForm" method="post">

                              <h4>User Information</h4>
                              <label>Username:</label>
                              <input type="text" name="user_name" value="<?=$resultUser[0]['user_name']?>" class="form_input required" placeholder="username"/>
                  
                              <label>Password:</label>
                              <input type="password" name="user_pass" value="<?=$resultUser[0]['user_pass']?>" class="form_input required" placeholder="password"/>
                  
                              <label>Nama Lengkap:</label>
                              <input type="text" name="user_nama" value="<?=$resultUser[0]['user_nama']?>" class="form_input required" placeholder="nama lengkap"/>
                  
                              <label>Email:</label>
                              <input type="text" name="user_email" value="<?=$resultUser[0]['user_email']?>" class="form_input required"placeholder="email" />
                  
                              <label>No HP:</label>
                              <input type="text" name="user_hp" value="<?=$resultUser[0]['user_hp']?>" class="form_input required" placeholder="no hp"/>

                              <label>Jenis Kelamin:</label>
                              <?php 
                              if($vtoken->data->utipe == "ADMIN")
                              {
                              ?>
                              <div class="selector_overlay">
                                 <select name="user_kelamin" class="cs-select cs-skin-overlay selectoptions">
                                 <?php
                                 $arr_gender = ["LAKI-LAKI","PEREMPUAN"];
                                 foreach ($arr_gender as $key => $value)
                                 {
                                    $selected = " ";
                                       if($resultUser[0]['user_kelamin'] == $key )
                                       {
                                       $selected = 'selected="selected"';
                                       }
                                       echo "<option value='".$key."' ".$selected ." >".$value."</option>" ;
                                 }
                                 ?>
                                 </select>
                                <?php
                                 }
                                 else
                                 {
                                    echo ' <label>'.$resultUser[0]['user_kelamin'].'</label>';
                                 }
                                ?>
                              </div>	

                           </div> 

                           <!-- <div class="table_section"><a href="#" class="col button button-large button-fill button-raised color-green">Update</a></div> -->
                           <br>
                           <label id="loader" style="display:none;"><img src="images/loader.gif" alt="Loading..." id="LoadingGraphic" /></label>

                           <div class="table_section"><input type="submit" name="submit" class="col button button-large button-fill button-raised color-green" id="submitProfile" value="Update" /></div>
                           <div class="table_section"><a href="#" onclick="toHome();"  class="col button button-outline">Cancel</a></div> 

                           </form>      
                          </div>
    
                          <div id="tsert" class="tab">
                          <h4>Skill Information</h4>
                          <?php $i=0; ?>
                          <ul class="responsive_table">
                                 <li class="table_row">
                                    <div class="table_section_small">No</div>
                                    <div class="table_section_14">Nama</div>
                                    <div class="table_section_14">Desc</div> 
                                    <div class="table_section_14">Status</div> 
                                 </li>
                                 <li class="table_row">
                                    <div class="table_section_14"><?=$i++;?></div>
                                    <div class="table_section_14">Qiro'ati</div>
                                    <div class="table_section_14">On Progress</div> 
                                    <div class="table_section_14">-</div> 
                                 </li>
                                 <li class="table_row">
                                    <div class="table_section_14"><?=$i++;?></div>
                                    <div class="table_section_14">Ummi</div>
                                    <div class="table_section_14">Jilid 1</div> 
                                    <div class="table_section_14">Verified</div> 
                                 </li>

                                 <li class="table_row">
                                    <div class="table_section"><a href="#" onclick="toHome();"  class="col button button-outline">Cancel</a></div> 
                                 </li>

                                 
                           </ul>
                          </div> 

                          <div id="tpayment" class="tab">
                           <h4>Payment Information</h4>
                           <?php $i=0; ?>
                              <ul class="responsive_table">
                                    <li class="table_row">
                                       <div class="table_section_small">No</div>
                                       <div class="table_section_14">BANK</div>
                                       <div class="table_section_14">No.Rekening</div> 
                                       <div class="table_section_14">Nama Pemilik</div> 
                                    </li>
                                    <li class="table_row">
                                       <div class="table_section_small"><?=$i++;?></div>
                                       <div class="table_section_14">BCA</div>
                                       <div class="table_section_14">141223344</div> 
                                       <div class="table_section_14">Agus Salim</div> 
                                    </li>
                                    <li class="table_row">
                                       <div class="table_section_small"><?=$i++;?></div>
                                       <div class="table_section_14">BNI</div>
                                       <div class="table_section_14">031112244</div> 
                                       <div class="table_section_14">Agus Salim</div> 
                                    </li>

                                    <li class="table_row">
                                       <div class="table_section"><a href="#" onclick="toHome();"  class="col button button-outline">Cancel</a></div> 
                                    </li>

                                    
                              </ul>
                          </div> 
                    </div>
              </div>
               
          <div class="close_loginpopup_button"><a href="#" class="close-popup"><img src="images/icons/white/menu_close.png" alt="" title="" /></a></div>
        </div>
    </div>
    <script>
   function toHome()
	{
		console.log("toHome");
    window.location="index.php";
	}
   </script>
