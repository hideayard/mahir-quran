<?php
if (session_status() === PHP_SESSION_NONE) {
    session_start();
}

// error_reporting(E_ALL ^ E_NOTICE);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require_once ('config/MysqliDb.php');
include_once ("config/db.php");
$db = new MysqliDb ('localhost', $dbuser, $dbpass, $dbname);
include("config/functions.php");
require_once ("jwt_token.php");
require_once ("customhelper.php");

date_default_timezone_set("Asia/Jakarta");
$tgl=date('Y-m-d');
$today=date('d M Y');
$page=isset($_GET['page']) ? $_GET['page'] : "home"; 
$mode=isset($_GET['mode']) ? $_GET['mode'] : ""; 
$d=isset($_GET['d']) ? $_GET['d'] : ""; 
$uid = 0;
$token = isset($_COOKIE['token']) ? $_COOKIE['token'] : false;
if($token)
{
  $vtoken = json_decode( verify_token($token) );
  if($vtoken->status)
  {
     $uid = $vtoken->data->uid;
  }
}


if( $uid != 0 )
{


$skrg = date("Y-m-d h:i:sa");

$id_user=$vtoken->data->uid;
$email=$vtoken->data->uemail;
$tipe_user=$vtoken->data->utipe;

$table = isset($_GET['t']) ? $_GET['t'] : 'inbox';
$selected=array();
$home='';
switch ($page) 
{
  case 'users' : {
                  $selected[8]=' class="active" ';$judul="Users";
                }break;
  case 'home' : {
                  $selected[4]=' class="active" ';$judul="Home";$home='active';
                }break;
  
  default : {
              $selected[4]=' class="active" ';$judul="Home";  
            }break;

}





$sql = "SELECT * FROM users WHERE user_id = '". $uid ."'"; 
$resultUser = $db->rawQuery($sql);//@mysql_query($sql);

}
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no, minimal-ui">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<link rel="apple-touch-icon" href="images/apple-touch-icon.png" />
<link rel="apple-touch-startup-image" media="(device-width: 320px) and (device-height: 568px) and (-webkit-device-pixel-ratio: 2)" href="apple-touch-startup-image-640x1096.png">
<title>Mahir Qur'an</title>
<link rel="stylesheet" href="css/framework7.css">
<link rel="stylesheet" href="style.css">
<link rel="stylesheet" href="css/colors/turquoise.css">
<link type="text/css" rel="stylesheet" href="css/swipebox.css" />
<link type="text/css" rel="stylesheet" href="css/animations.css" />
<link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,300,700,900' rel='stylesheet' type='text/css'>
<style>
  .circle-image{
  display: inline-block;
  border-radius: 50%;
  overflow: hidden;
  width: 250px;
  height: 250px;
}
.circle-image img{
  width:100%;
  height:100%;
  object-fit: cover;
}

.center {
  margin: auto;
  text-align:center;
}
</style>
</head>
<body id="mobile_wrap">

    <div class="statusbar-overlay"></div>

    <div class="panel-overlay"></div>

    

    <?php include_once("left_panel.php"); ?>


    <div class="views">

      <div class="view view-main">

        <div class="pages  toolbar-through">

          <div data-page="index" class="page homepage">
            <div class="page-content">
            </div>
          </div>
        </div>
        <!-- Bottom Toolbar-->
        <div class="toolbar">
              <div class="toolbar-inner">
              <ul class="toolbar_icons">
              <li><a href="#" data-panel="left" class="open-panel"><img src="images/icons/white/menu.png" alt="" title="" /></a></li>
              <li><a href="booking.php"><img src="images/icons/white/blog.png" alt="" title="" /></a></li>
              
              <li class="menuicon"><a href="menu.php"><img src="images/icons/white/home.png" alt="" title="" /></a></li>

              <li><a href="contact.php"><img src="images/icons/white/contact.png" alt="" title="" /></a></li>
              <?php
              if(isset($_SESSION['u']))
              {
                

                echo '<li><a href="profile.php"><img src="images/icons/white/user.png" alt="" title="" /></a></li>';
                // echo '<li><a href="#" data-popup=".popup-profile" class="open-popup" ><img src="images/icons/white/user.png" alt="" title="" /></a></li>';
              }
              else{
                echo '<li><a href="menu.php"><img src="images/icons/white/user.png" alt="" title="" /></a></li>';
              }
              ?>
                            </ul>
              </div>  
        </div>
      </div>
    </div>

    
<?php include_once("login.php"); ?>
    
<?php include_once("register.php"); ?>

<?php
  // if($uid != 0)
  // {
  //   // include_once("profile.php");
  // }
  ?>

<?php include_once("forgot.php"); ?>
 
    <!-- Social Popup -->
    <div class="popup popup-social">
    <div class="content-block">
      <h4>Follow Us</h4>
      <p>Social icons solution that allows you share and increase your social popularity.</p>
      <ul class="social_share">
      <li><a href="#"><img src="images/icons/white/twitter.png" alt="" title="" /></a></li>
      <li><a href="#"><img src="images/icons/white/facebook.png" alt="" title="" /></a></li>
      <li><a href="#"><img src="images/icons/white/googleplus.png" alt="" title="" /></a></li>
      <li><a href="#"><img src="images/icons/white/dribbble.png" alt="" title="" /></a></li>
      <li><a href="#"><img src="images/icons/white/linkedin.png" alt="" title="" /></a></li>
      <li><a href="#"><img src="images/icons/white/pinterest.png" alt="" title="" /></a></li>
      </ul>
      <div class="close_popup_button"><a href="#" class="close-popup"><img src="images/icons/white/menu_close.png" alt="" title="" /></a></div>
    </div>
    </div>
    
<script type="text/javascript" src="js/jquery-1.10.1.min.js"></script>
<script src="js/jquery.validate.min.js" type="text/javascript"></script>
<script type="text/javascript" src="js/framework7.js"></script>
<script type="text/javascript" src="js/classie.js"></script>
<script type="text/javascript" src="js/selectFx.js"></script>
<script type="text/javascript" src="js/my-app.js"></script>
<script type="text/javascript" src="js/jquery.swipebox.js"></script>
<script type="text/javascript" src="js/email.js"></script>
<script type="text/javascript" src="js/login.js"></script>
<script type="text/javascript" src="js/logout.js"></script>
<script type="text/javascript" src="js/register.js"></script>
<script type="text/javascript" src="js/profile.js"></script>

<script type="text/javascript" >
let token = getCookie('token');
	if(token)
	{
    console.log('index token=',parseJwt(token) );
		// myApp.alert('token=',parseJwt(token) );
	}
</script>
  </body>
</html>