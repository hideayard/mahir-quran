

<?php
if (session_status() === PHP_SESSION_NONE) {
   session_start();
}
// error_reporting(E_ALL ^ E_NOTICE);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

if(!isset($_SESSION['u']))
{
   echo '<script>window.location="index.php";</script>';
}

require_once ('config/MysqliDb.php');
include_once ("config/db.php");
$db = new MysqliDb ('localhost', $dbuser, $dbpass, $dbname);
include("config/functions.php");
require_once ("jwt_token.php");
require_once ("customhelper.php");

$vtoken = json_decode( verify_token($_COOKIE['token']) );

if($vtoken->data->uid)
{
   $uid = $vtoken->data->uid;
}
else
{
   $uid = 0;
}

 $sql = "SELECT * FROM users WHERE user_id = '". $uid ."'"; 
 $result = $db->rawQuery($sql);//@mysql_query($sql);

 $sql2 = "SELECT * FROM users_skill WHERE user_id = '". $uid ."'"; 
 $users_skill = $db->rawQuery($sql2);//@mysql_query($sql);

 $sql3 = "SELECT * FROM users_bank_account WHERE user_id = '". $uid ."' and status = 1"; 
 $users_bank_account = $db->rawQuery($sql3);//@mysql_query($sql);
 $i=1;
?>
<div class="pages">
  <div data-page="projects" class="page no-toolbar no-navbar">
    <div class="page-content">
    
     <div class="navbarpages">
       <div class="nav_left_logo"><a href="index.php"><img src="images/logo.png" alt="" title="" /></a></div>
       <div class="nav_right_button"><a href="menu.php"><img src="images/icons/white/menu.png" alt="" title="" /></a></div>
     </div>
     <div id="pages_maincontent">
     
              <h2 class="page_title">Profile User <?=$result[0]['user_name']?></h2> 

              <div class="page_content center"> 
            
              <span class="circle-image ">
                  <img src="uploads/user/<?=$result[0]['user_foto']?>" alt="profile" />
              </span>
              			                
            <hr>
                <div class="buttons-row">
                    <a href="#tinfo" class="tab-link active button">User Info</a>
                    <a href="#tsert" class="tab-link button">Skill</a>
                    <a href="#tpayment" class="tab-link button">Payment</a>
              </div>
              
              <div class="tabs-animated-wrap">
                    <div class="tabs">
                          <div id="tinfo" class="tab active">
                           <h4>User Information</h4>
                           <h2 id="Note"></h2>
                              <div class="loginform">
                                 <form id="ProfileForm" method="post" enctype='multipart/form-data'>
                                 <input type="hidden" name="token" value="<?=$_COOKIE['token']?>" />
                                 <input type="hidden" name="user_id" value="<?=$uid?>" />
                                 <input type="hidden" name="mode" value="update" />
                                 
                                    <label>Username:</label>
                                    <input type="text" name="user_name" value="<?=$result[0]['user_name']?>" class="form_input required" placeholder="username"/>

                                    <label>Password:</label>
                                    <input type="password" name="user_pass" value="" class="form_input" placeholder="******"/>
                        
                                    <label>Nama Lengkap:</label>
                                    <input type="text" name="user_nama" value="<?=$result[0]['user_nama']?>" class="form_input required" placeholder="nama lengkap"/>
                        
                                    <label>Email:</label>
                                    <input type="text" name="user_email" value="<?=$result[0]['user_email']?>" class="form_input required"placeholder="email" />
                        
                                    <label>No HP:</label>
                                    <input type="text" name="user_hp" value="<?=$result[0]['user_hp']?>" class="form_input required" placeholder="no hp"/>

                                    <label>Jenis Kelamin:</label>
                                    <?php 
                                    if($vtoken->data->utipe == "ADMIN")
                                    {
                                    ?>
                                    <div class="selector_overlay">
                                       <select name="user_kelamin" class="cs-select cs-skin-overlay selectoptions">
                                          
                                       <?php
                                       $arr_gender = ["LAKI-LAKI","PEREMPUAN"];
                                       foreach ($arr_gender as $key => $value)
                                       {
                                          $selected = " ";
                                             if($result[0]['user_kelamin'] == $value )
                                             {
                                             $selected = 'selected="selected"';
                                             }
                                             echo "<option ".$selected ." >".$value."</option>" ;
                                       }
                                       ?>
                                       </select>
                                    <?php
                                       }
                                       else
                                       {
                                          echo ' <label>'.$result[0]['user_kelamin'].'</label>';
                                       }
                                    ?>
                                    </div>	
                                    <hr>
                                    <label>Photo:</label>
                                    <input type="file" id="user_foto" class="form_input" placeholder="user_foto" />

                                    <br>

                                    <div class="table_section"><input type="submit" name="submit" class="col button button-large button-fill button-raised color-green" id="submitProfile" value="Update" /></div>
                                    <div class="table_section"><a href="#" onclick="toHome();"  class="col button button-outline">Cancel</a></div> 
                                    
                                    <label id="loader" style="display:none;"><img src="images/loader.gif" alt="Loading..." id="LoadingGraphic" /></label>
                                 </form>
                              </div>
                           </div>

                          <div id="tsert" class="tab">
                              <h4>Skill Information</h4>
                              <?php $i=0; ?>
                              <ul class="responsive_table">
                                 <li class="table_row">
                                    <div class="table_section_small">No</div>
                                    <div class="table_section_14">Nama</div>
                                    <div class="table_section_14">Desc</div> 
                                    <div class="table_section_14">Status</div> 
                                 </li>
                                 <?php
                                 $j=0;
                                 foreach($users_skill as $value)
                                 {
                                    $vr = "-";
                                    if($value['is_verified'] == 1)
                                    {
                                       $vr = "<a>Verified</a>";
                                    }
                                    echo '<li class="table_row">
                                             <div class="table_section_small">'.++$j.'</div>
                                             <div class="table_section_14">'.$value['nama'].'</div>
                                             <div class="table_section_14">'.$value['progress'].'</div> 
                                             <div class="table_section_14">'.$vr.'</div> 
                                          </li>';
                                 }
                                 ?>
                                      

                                       <li class="table_row">
                                          <div class="table_section"><a href="#" onclick="toHome();"  class="col button button-outline">Cancel</a></div> 
                                       </li>

                                 </ul>
                          </div> 

                          <div id="tpayment" class="tab">
                           <h4>Payment Information</h4>
                           <?php $i=0; ?>
                              <ul class="responsive_table">
                                    <li class="table_row">
                                       <div class="table_section_small">No</div>
                                       <div class="table_section_14">BANK</div>
                                       <div class="table_section_14">No.Rekening</div> 
                                       <div class="table_section_14">Nama Pemilik</div> 
                                    </li>

                                    <?php
                                       $j=0;
                                       foreach($users_bank_account as $value)
                                       {
                                          echo '<li class="table_row">
                                                   <div class="table_section_small">'.++$j.'</div>
                                                   <div class="table_section_14">'.$value['nama_bank'].'</div>
                                                   <div class="table_section_14">'.$value['no_rekening'].'</div> 
                                                   <div class="table_section_14">'.$value['atas_nama'].'</div> 
                                                </li>';
                                       }
                                    ?>


                                    <li class="table_row">
                                       <div class="table_section"><a href="#" onclick="toHome();"  class="col button button-outline">Cancel</a></div> 
                                    </li>

                                    
                              </ul>
                          </div> 
                    </div>
              </div>
              
              </div>
      
      </div>
      
      
    </div>
  </div>
</div>

