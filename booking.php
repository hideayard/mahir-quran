<?php
if (session_status() === PHP_SESSION_NONE) {
  session_start();
}
require_once ('config/MysqliDb.php');
include_once ("config/db.php");
$db = new MysqliDb ('localhost', $dbuser, $dbpass, $dbname);
 $sql = "SELECT * FROM metode WHERE status = 1 "; 
$resultbooking = $db->rawQuery($sql);//@mysql_query($sql);
 $i=1;

 $gender = isset($_SESSION['gender'])?$_SESSION['gender']:'LAKI-LAKI';
 
?>

<div class="pages">
  <div data-page="projects" class="page no-toolbar no-navbar">
    <div class="page-content">
    
     <div class="navbarpages">
       <div class="nav_left_logo"><a href="index.php"><img src="images/logo.png" alt="" title="" /></a></div>
       <div class="nav_right_button">
       <a href="menu.php"><img src="images/icons/white/menu.png" alt="" title="" /></a>
       <a href="#" data-panel="right" class="open-panel"><img src="images/icons/white/search.png" alt="" title="" /></a>
       </div>
     </div>
     <div id="pages_maincontent">
      
      <h2 class="page_title">Booking Jadwal Guru</h2>
      
       <div class="page_content"> 
            <div class="loginform">
                <form id="CariJadwalForm" method="post" action="index.php">
               
                <div class="item-content item-input form_input">
                    <div class="item-inner">
                    <div class="item-title item-label">Tanggal</div>
                    <div class="item-input-wrap">
                        <input class="form_input" type="date" placeholder="Tanggal" value="<?=date('Y-m-d')?>"/>
                    </div>
                    </div>
                </div>

                <div class="item-content item-input form_input">
                    <div class="item-inner">
                    <div class="item-title item-label">Jam</div>
                    <div class="item-input-wrap">
                        <input class="form_input" type="time" placeholder="Jam" value=""/>
                    </div>
                    </div>
                </div>

                <div class="item-content item-input form_input">
                    <div class="item-inner">
                    <div class="item-title item-label">Metode</div>
                    <div class="item-input-wrap">
                        <select name="metode" class="cs-select cs-skin-overlay selectoptions">
                            <?php
                            foreach($resultbooking as $key => $value)
                            {
                            echo "<option value=\"".$value['nama']."\">".$value['label']."</option>";
                            }
                            ?>
                        </select>
                    </div>
                    </div>
                </div>
                    
                    <label id="loader" style="display:none;"><img src="images/loader.gif" alt="Loading..." id="LoadingGraphic" /></label>
                    <input type="submit" name="submit" class="form_submit" id="submitLogin" value="Cari Guru" />
                    <hr>
                    <a onclick="toHome();" href="#" class="col button button-large button-fill button-raised color-green">Cancel</a>

                </form>
            </div>
        </div>
      <hr>
            <div class="list-block">
              <ul class="posts">
                <?php
                if($gender== "LAKI-LAKI") 
                {
                ?>


                <li class="swipeout">
                  <div class="swipeout-content item-content">
                    <div class="post_entry">
                        <div class="post_date">
                            <span class="day">01</span>
                            <span class="month">Juli</span>
                        </div>
                        <div class="post_title">
                        <h2><a href="#">Ustadz Achmad Al Ghifary (Iqro')</a></h2>
                        
                        </div>
                        <div class="post_swipe"><img src="images/swipe_more.png" alt="" title="" /></div>
                    </div>
                  </div>
                  <div class="swipeout-actions-right">
                    <a href="#" class="action1"><img src="images/icons/white/message.png" alt="" title="" /></a>
                    <a href="#" class="action1 open-popup" data-popup=".popup-social"><img src="images/icons/white/like.png" alt="" title="" /></a>
                    <a href="#" class="action1 open-popup" data-popup=".popup-social"><img src="images/icons/white/contact.png" alt="" title="" /></a>
                  </div>
                </li>  

                <li class="swipeout">
                  <div class="swipeout-content item-content">
                    <div class="post_entry">
                        <div class="post_date">
                             <span class="day">09</span>
                            <span class="month">Juli</span>
                        </div>
                        <div class="post_title">
                        <h2><a href="#">Ustadz Arif Rahman H. (Tsaqifa)</a></h2>
                        </div>
                        <div class="post_swipe"><img src="images/swipe_more.png" alt="" title="" /></div>
                    </div>
                  </div>
                  <div class="swipeout-actions-right">
                    <a href="#" class="action1"><img src="images/icons/white/message.png" alt="" title="" /></a>
                    <a href="#" class="action1 open-popup" data-popup=".popup-social"><img src="images/icons/white/like.png" alt="" title="" /></a>
                    <a href="#" class="action1 open-popup" data-popup=".popup-social"><img src="images/icons/white/contact.png" alt="" title="" /></a>
                  </div>
                </li> 

                <li class="swipeout">
                  <div class="swipeout-content item-content">
                    <div class="post_entry">
                        <div class="post_date">
                            <span class="day">13</span>
                            <span class="month">Juli</span>
                        </div>
                        <div class="post_title">
                        <h2><a href="#">Ustadz Hani Arrifa'i (Yanbu'a)</a></h2>
                        </div>
                        <div class="post_swipe"><img src="images/swipe_more.png" alt="" title="" /></div>
                    </div>
                  </div>
                  <div class="swipeout-actions-right">
                    <a href="#" class="action1"><img src="images/icons/white/message.png" alt="" title="" /></a>
                    <a href="#" class="action1 open-popup" data-popup=".popup-social"><img src="images/icons/white/like.png" alt="" title="" /></a>
                    <a href="#" class="action1 open-popup" data-popup=".popup-social"><img src="images/icons/white/contact.png" alt="" title="" /></a>
                  </div>
                </li> 
                <li class="swipeout">
                  <div class="swipeout-content item-content">
                    <div class="post_entry">
                        <div class="post_date">
                            <span class="day">15</span>
                            <span class="month">Juli</span>
                        </div>
                        <div class="post_title">
                        <h2><a href="#">Ustadz Muhammad Fadil. (Qiro'ati)</a></h2>
                        </div>
                        <div class="post_swipe"><img src="images/swipe_more.png" alt="" title="" /></div>
                    </div>
                  </div>
                  <div class="swipeout-actions-right">
                    <a href="#" class="action1"><img src="images/icons/white/message.png" alt="" title="" /></a>
                  <a href="#" class="action1 open-popup" data-popup=".popup-social"><img src="images/icons/white/like.png" alt="" title="" /></a>
                  <a href="#" class="action1 open-popup" data-popup=".popup-social"><img src="images/icons/white/contact.png" alt="" title="" /></a>
                  </div>
                </li> 


                <?php
                }
                else
                {
                ?>
               
                <li class="swipeout">
                  <div class="swipeout-content item-content">
                    <div class="post_entry">
                        <div class="post_date">
                            <span class="day">02</span>
                            <span class="month">Juli</span>
                        </div>
                        <div class="post_title">
                        <h2><a href="#">Ustadzah Oky Setiana (Ummi)</a></h2>
                        </div>
                        <div class="post_swipe"><img src="images/swipe_more.png" alt="" title="" /></div>
                    </div>
                  </div>
                  <div class="swipeout-actions-right">
                    <a href="#" class="action1"><img src="images/icons/white/message.png" alt="" title="" /></a>
                    <a href="#" class="action1 open-popup" data-popup=".popup-social"><img src="images/icons/white/like.png" alt="" title="" /></a>
                    <a href="#" class="action1 open-popup" data-popup=".popup-social"><img src="images/icons/white/contact.png" alt="" title="" /></a>
                  </div>
                </li> 
                		
                <li class="swipeout">
                  <div class="swipeout-content item-content">
                    <div class="post_entry">
                        <div class="post_date">
                            <span class="day">11</span>
                            <span class="month">Juli</span>
                        </div>
                        <div class="post_title">
                        <h2><a href="#">Ustadzah Aisyah. (Tilawati)</a></h2>
                        </div>
                        <div class="post_swipe"><img src="images/swipe_more.png" alt="" title="" /></div>
                    </div>
                  </div>
                  <div class="swipeout-actions-right">
                    <a href="#" class="action1"><img src="images/icons/white/message.png" alt="" title="" /></a>
                    <a href="#" class="action1 open-popup" data-popup=".popup-social"><img src="images/icons/white/like.png" alt="" title="" /></a>
                    <a href="#" class="action1 open-popup" data-popup=".popup-social"><img src="images/icons/white/contact.png" alt="" title="" /></a>
                  </div>
                </li> 
                
                <li class="swipeout">
                  <div class="swipeout-content item-content">
                    <div class="post_entry">
                        <div class="post_date">
                            <span class="day">14</span>
                            <span class="month">Juli</span>
                        </div>
                        <div class="post_title">
                        <h2><a href="#">Ustadzah Isti Isnawati. (Qiro'ati)</a></h2>
                        </div>
                        <div class="post_swipe"><img src="images/swipe_more.png" alt="" title="" /></div>
                    </div>
                  </div>
                  <div class="swipeout-actions-right">
                    <a href="#" class="action1"><img src="images/icons/white/message.png" alt="" title="" /></a>
                    <a href="#" class="action1 open-popup" data-popup=".popup-social"><img src="images/icons/white/like.png" alt="" title="" /></a>
                    <a href="#" class="action1 open-popup" data-popup=".popup-social"><img src="images/icons/white/contact.png" alt="" title="" /></a>
                  </div>
                </li>  
                <?php
                }
                ?>
              </ul>
              
            <div class="clear"></div>  
            <div id="loadMore"><img src="images/load_posts.png" alt="" title="" /></div> 
            <div id="showLess"><img src="images/load_posts_disabled.png" alt="" title="" /></div> 
            </div>
      
      </div>
      
      </div>
      
      
    </div>
  </div>
</div>

<script>
function toHome()
{
    console.log("toHome");
    window.location="index.php";
}
</script>